﻿using System;
using System.Collections.Generic;
using CustomParser;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace CustomParserTests
{
    [TestFixture]
    public class IntParserTests
    {
        public static IEnumerable<TestCaseData> TestCasesParse
        {
            get
            {
                yield return new TestCaseData("0").Returns(0);
                yield return new TestCaseData("10").Returns(10);
                yield return new TestCaseData("123456").Returns(123456);
                yield return new TestCaseData("-10").Returns(-10);
            }
        }
        [Test, TestCaseSource(nameof(TestCasesParse))]
        public int TestParse(string str)
        {
            return IntParser.Parse(str);
        }

        public static IEnumerable<TestCaseData> TestCasesParseThrowsFormatException
        {
            get
            {
                yield return new TestCaseData("fs0495sf");
                yield return new TestCaseData(" gjlkfdjg");
                yield return new TestCaseData("-1kdsjfh1");
                yield return new TestCaseData("767 3476");
            }
        }

        [Test, TestCaseSource(nameof(TestCasesParseThrowsFormatException))]
        public void TestParseThrowsFormatException(string str)
        {
            Assert.That(() => IntParser.Parse(str), Throws.TypeOf<FormatException>());
        }

        public static IEnumerable<TestCaseData> TestCasesParseThrowsArgumentException
        {
            get
            {
                yield return new TestCaseData("");
                yield return new TestCaseData(null);
            }
        }
        [Test, TestCaseSource(nameof(TestCasesParseThrowsArgumentException))]
        public void TestParseThrowsArgumentException(string str)
        {
            Assert.That(() => IntParser.Parse(str), Throws.TypeOf<ArgumentException>());
        }

        public static IEnumerable<TestCaseData> TestCasesParseThrowsOverflowException
        {
            get
            {
                yield return new TestCaseData("2000000000000");
            }
        }
        [Test, TestCaseSource(nameof(TestCasesParseThrowsOverflowException))]
        public void TestParseThrowsOverflowException(string str)
        {
            Assert.That(() => IntParser.Parse(str), Throws.TypeOf<OverflowException>());
        }

        public static IEnumerable<TestCaseData> TestCasesTryParse
        {
            get
            {
                yield return new TestCaseData("10").Returns(true);
                yield return new TestCaseData("1f0").Returns(false);
            }
        }
        [Test, TestCaseSource(nameof(TestCasesTryParse))]
        public bool TestTryParse(string str)
        {
            int res = 0;
            return IntParser.TryParse(str, out res);
        }
    }
}
