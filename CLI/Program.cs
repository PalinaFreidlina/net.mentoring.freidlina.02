﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                FirstSymbolMenu();
            } while (true);
        }
        
        static void FirstSymbolMenu()
        {
            Console.WriteLine("\nInput lines (press ESC to finish)...");
            List<string> lines = new List<string>();
            while (Console.ReadKey(true).Key != ConsoleKey.Escape)
            {
                var line = Console.ReadLine();
                if (string.IsNullOrEmpty(line))
                {
                    Console.WriteLine("!!!Line is empty!!!");
                    continue;
                }
                lines.Add(line);
            } 
            Console.WriteLine($"Result: {string.Join(", ", lines.Select(x => x[0]))}");
        }
        
    }
}
