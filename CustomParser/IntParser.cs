﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomParser
{
    public class IntParser
    {
        public static int Parse(string str)
        {
            if (string.IsNullOrEmpty(str)) throw new ArgumentException();
            string bufferString = str;
            int sign = bufferString[0] == '-' ? -1 : 1;
            if (sign == -1) bufferString = new string(bufferString.Skip(1).ToArray()); 
            var bitsNumber = bufferString.Length-1;
            int result = 0;
            foreach (var ch in bufferString)
            {
                if (char.IsDigit(ch))
                {
                    try
                    {
                        checked
                        {
                            result += (ch - '0')*(int) Math.Pow(10, bitsNumber--);
                        }
                    }
                    catch (OverflowException e)
                    {
                        throw e;
                    }
                } 
                else
                    throw new FormatException();
            }
            return result*sign;
        }

        public static bool TryParse(string str, out int result)
        {
            try
            {
                result = Parse(str);
                return true;
            }
            catch (Exception)
            {
                result = 0;
                return false;
            }
        }
    }
}
