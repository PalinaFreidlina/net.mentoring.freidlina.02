﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    public class TextProcessor
    {
        public static string LinesFirstSymbols(IEnumerable<string> lines)
        {
            return string.Join(", ", lines.Select(x => x[0]));
        }
    }
}
